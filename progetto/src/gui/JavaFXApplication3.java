/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 *
 * @author Andrea
 */
public class JavaFXApplication3 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        Nota n = new Nota("05-02-1992","dvvv", "prova");
        ArrayList<Nota> listanota = new ArrayList<>(); 
        listanota.add(n);
        Button Nuovo = new Button();
        Button Salva = new Button();
        Button Elimina = new Button();
        Button Annulla = new Button();
        TextField Titolo= new TextField();
        TextArea Note= new TextArea();
        TableView<Nota> Table= new TableView<>();
        ObservableList<Nota> olistanota= FXCollections.observableArrayList(listanota);
        Table.setItems(olistanota);
        TableColumn<Nota,String> DataCol = new TableColumn<>("Data");
        DataCol.setCellValueFactory(new PropertyValueFactory<>("Data"));
        TableColumn<Nota,String> TitoloCol = new TableColumn<>("Titolo");
        TitoloCol.setCellValueFactory(new PropertyValueFactory<>("Titolo"));
        Table.getColumns().setAll(DataCol, TitoloCol);
        
        
        Nuovo.setText("Nuovo");
        Salva.setText("Salva");
        Elimina.setText("Elimina");
        Annulla.setText("Annulla");
        Nuovo.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Nuovo");
            }
        });
        Group root= new Group(Titolo, Note, Nuovo, Salva, Elimina, Annulla, Table);
        
        Table.setMinSize(390, 450);
        Nuovo.setLayoutX(400);
        Nuovo.setLayoutY(10);
        Salva.setLayoutX(400);
        Salva.setLayoutY(360);
        Elimina.setLayoutX(520);
        Elimina.setLayoutY(10);
        Annulla.setLayoutX(520);
        Annulla.setLayoutY(360);
        Titolo.setLayoutX(400);
        Titolo.setLayoutY(50);
        Note.setLayoutX(400);
        Note.setLayoutY(100);
        Titolo.setMinWidth(100);
        Note.setMinHeight(250);
        Note.setMaxWidth(250);
        Scene scene = new Scene(root, 700, 450);
        
        primaryStage.setTitle("Notes");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
